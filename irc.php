<?php
  $tpl["page"] = "irc";
  $tpl["css"] = "css/main.css?";
  require("/home/static/public_html/header.php");
  require("/home/static/public_html/header_body.php");
?>
<div style="padding: 20px;">
  <div style="border: 1px solid #ddd; background-color: #eee; border-radius: 4px; padding: 20px;">
  <div style="font-size: 18px; font-weight: bold; padding-bottom: 15px;">
    کانال گپ و گفتگوی کاربران ایرانی اوبونتو
  </div>
  <p>
    در این صفحه و پس از ورود به کانال، میتوانید به گپ و گفتگوی زنده با کاربران
    اوبونتو بپردازید، اشکالات احتمالی خود را مطرح نموده و یا راجع به مسائل
    متنوع روزمره و اتفاقات دنیای گنو-لینوکس و نرم‌افزارهای آزاد صحبت کنید.
    در صورتی که علاقه دارید در گروه به صورت دائم بمانید، می‌توانید از
    نرم‌افزار Matrix استفاده کنید. برای اطلاعات بیشتر به
    <a href="https://wiki.ubuntu.ir/wiki/Chat">ویکی</a>
    مراجعه کنید.</p>
    <iframe width=100% height=600 scrolling=no style="border:0" src="https://web.libera.chat/#ubuntu-ir"></iframe>
  </div>
</div>
<?php
        require("/home/static/public_html/footer_body.php");
        require("/home/static/public_html/footer.php");
?>
