<?php
  $tpl["page"] = "main";
  $tpl["css"] = "css/main.css?";
  require("/home/static/public_html/header.php");
  require("/home/static/public_html/header_body.php");
?>
  <div class="showcase">
    <div class="container">
      <div class="row">
        <div class="col-md-6 text-white">
          <h1>
            اوبونتو ۲۲.۰۴ این‌جاست!
          </h1>
          <h3>
            جدیدترین نگارش اوبونتو با پشتیبانی طولانی‌مدت را برای
            رایانه‌ها، کارسازها و ابرواره‌های خود دریافت کنید.
          </h3>
          <a class="button" href="http://www.ubuntu.com/download/">
            دریافت اوبونتو ۲۲.۰۴
          </a>
        </div>
        <div class="col-md-6">
          <img class="image" src="images/showcase.png" alt="ویترین">
        </div>
      </div><!--row-->
    </div><!--container-->
  </div><!--showcase-->
  <div class="container">
    <div class="row grid">
      <div class="col-sm">
	<h3><a href="https://wiki.ubuntu.ir/wiki/Ubuntu">
        اوبونتو چیست&nbsp;&rsaquo;
        </a></h3>
	<p>
        سیستم‌عامل آزاد پیشرو برای رایانه‌ها، تلفن‌ها،
        رایانک‌ها و کارسازها با بیش از ۴۰ میلیون کاربر.
        </p>
        <div class="center">
          <img alt="اوبونتو" src="images/logo-ubuntu_cof-orange-hex.svg" style="width: 50%;">
        </div>
      </div>
      <div class="col-sm">
	<h3><a href="http://forum.ubuntu.ir">
        انجمن‌های فارسی اوبونتو&nbsp;&rsaquo;
        </a></h3>
	<p>
	پرسشی دارید؟ همین حالا بپرسید.
        جامعهٔ کاربران اوبونتو ایران، آمادهٔ پاسخگویی به شماست.
        </p>
        <div class="center">
          <img alt="انجمن‌ها" src="images/pictogram-forum-orange-hex.svg" style="width: 50%;">
        </div>
      </div>
      <div class="col-sm">
	<h3><a href="http://www.ubuntu.com/download">
        دریافت اوبونتو&nbsp;&rsaquo;
        </a></h3>
	<p>
        اوبونتو ۲۲.۰۴ با آخرین برنامه‌ها و بیش‌ترین
        امکانات و همچنین با پشتیبانی طولانی‌مدّت.</p>
        <div class="center">
          <img alt="دریافت" src="images/pictogram-download-orange-hex.svg" style="width: 50%;">
        </div>
      </div>
    </div><!--row-->
    <br>
  </div><!--container-->
<?php
  require("/home/static/public_html/footer_body.php");
  require("/home/static/public_html/footer.php");
?>
